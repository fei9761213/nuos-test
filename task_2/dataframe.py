import numpy as np
import pandas as pd
import seaborn as sns


class DF(pd.DataFrame):
    """A class that extends pandas.core.frame.DataFrame with additional methods to enhance its quality of life for various operations.

    Args:
        input (str or pd.DataFrame): If input type is str, a new DataFrame is created; otherwise, it's copied.

    Attributes:
        rows (int): Number of rows in the DataFrame.
        cols (int): Number of columns in the DataFrame.
    
    Properties:
        _constructor: Returns the constructor of the class DF.

    Methods:
        feature_engineering(prefix: int, zone_path: str, sort_columns: bool = False): Performs feature engineering on the DataFrame.
        drop_keys(drop_na: bool = True, keys: list = None): Drops specified keys and NaN from the DataFrame.
        clean(keys: list, method: str): Cleans the DataFrame based on specified keys and cleaning method.
        outlier(key: str): Removes outliers from the DataFrame based on the specified key.
        plot_hist(key: str, data: pd.DataFrame = None): Plots a histogram of the specified key in the DataFrame.
        plot_dist(x_key: str, y_key: str): Plots a distribution plot of two specified keys in the DataFrame.
        plot_heat(): Plots a heatmap of DataFrame.
        get_mean(key: str, group_by: str = None): Returns the mean of the specified key in the DataFrame.
        get_median(key: str, group_by: str = None): Returns the median of the specified key in the DataFrame.
        get_mode(key: str, group_by: str = None): Returns the mode of the specified key in the DataFrame.
        get_stdv(key: str, group_by: str = None): Returns the standard deviation of the specified key in the DataFrame.

    Private Methods:
        __chronological(keys: list): Rearranges DataFrame columns that related to time.
        __locational(zone_path: str, keys: list): Maps pickup and dropoff zones ID to zone name.
        __money(): Calculates tip percent based on tip amount and total amount.
        __dropna(): Drops rows and columns with NaN values.
        __update(): Updates the 'rows' and 'cols' attributes based on DataFrame shape.
    """
    
    def __init__(self, input, *args, **kwargs):
        """Constructor to initialize the attributes of the class.

        Args:
            input (str or pd.DataFrame): If input type is str, creates a new DataFrame by reading a Parquet file using PyArrow engine; 
                otherwise, copies the provided DataFrame.
        """
        if type(input) is str:
            if ".parquet" in input:
                _df = pd.read_parquet(input, engine="pyarrow")
            elif ".csv" in input:
                _df = pd.read_csv(input)
        else:
            _df = input
        super().__init__(_df, *args, **kwargs)

        self.__update()

    @property
    def _constructor(self):
        """Returns the constructor of the class DF."""
        return DF

    def feature_engineering(self, sort_columns: bool = False):
        """Performs feature engineering on the DataFrame.

        Args:
            prefix (int): Prefix indicating the datetime keys to use.
            zone_path (str): Path to the zone lookup table.
            sort_columns (bool, optional): Whether to sort columns alphabetically. Defaults to False.
        """
        self = pd.get_dummies(self)

        if sort_columns:
            self.reindex(sorted(self.columns), axis=1)
        self.__update()
        return self

    def drop_keys(self, drop_na: bool = True, keys: list = None):
        """Drops specified keys from the DataFrame.

        Args:
            drop_na (bool, optional): Whether to drop rows with NaN values. Defaults to True.
            keys (list, optional): List of keys to drop. Defaults to None.
        """
        if drop_na:
            self.__dropna()
        if keys is not None:
            self.drop(keys, axis=1, inplace=True)
        self.__update()

    def clean(self, keys: list, method: str):
        """Cleans the DataFrame based on specified keys and cleaning method.

        Args:
            keys (list): List of keys to clean.
            method (str): Cleaning method, either 'absolute' or 'median'.
        """
        if method == "absolute":
            for key in keys:
                self[key] = self[key].abs()
        elif method == "median":
            for key in keys:
                median = self.get_median(key)
                self[key] = self[key].mask(self[key] == 0, median)
        self.__update()

    def outlier(self, keys: list):
        """Removes outliers from the DataFrame based on the specified key.

        Args:
            key (str): Key to identify the column containing outliers.
        """
        for key in keys:
            mean = self.get_mean(key)
            stdv = self.get_stdv(key)
            zscore = (self[key] - mean) / stdv

            self.drop(zscore[zscore > 3].index, inplace=True)

    def plot_hist(self, key: str, data: pd.DataFrame = None):
        """Plots a histogram of the specified key in the DataFrame.

        Args:
            key (str): Key to identify the column to plot.
            data (pd.DataFrame, optional): DataFrame to use for plotting. Defaults to None.
        """

        if data is None:
            data = self
        
        sns.distplot(data[key], kde=True)

    def plot_dist(self, x_key: str, y_key: str):
        """Plots a distribution plot of two specified keys in the DataFrame.

        Args:
            x_key (str): Key for the x-axis.
            y_key (str): Key for the y-axis.
        """
        sns.displot(data=self, x=x_key, y=y_key, kind="kde")

    def plot_heat(self):
        """Plots a heatmap of the transposed DataFrame."""
        sns.heatmap(self, cmap="coolwarm", cbar=True)

    def get_mean(self, key: str, group_by: str = None):
        """Returns the mean of the specified key in the DataFrame.

        Args:
            key (str): Key to identify the column.
            group_by (str, optional): Key to group by. Defaults to None.

        Returns:
            float or pd.Series: Mean value(s).
        """
        if group_by is None:
            return self[key].mean()
        else:
            return self.groupby(group_by)[key].mean()

    def get_median(self, key: str, group_by: str = None):
        """Returns the median of the specified key in the DataFrame.

        Args:
            key (str): Key to identify the column.
            group_by (str, optional): Key to group by. Defaults to None.

        Returns:
            float or pd.Series: Median value(s).
        """
        if group_by is None:
            return self[key].median()
        else:
            return self.groupby(group_by)[key].median()

    def get_mode(self, key: str, group_by: str = None):
        """Returns the mode of the specified key in the DataFrame.

        Args:
            key (str): Key to identify the column.
            group_by (str, optional): Key to group by. Defaults to None.

        Returns:
            pd.Series: Mode value(s).
        """
        if group_by is None:
            return self[key].mode()
        else:
            return self.groupby(group_by)[key].agg(pd.Series.mode)

    def get_stdv(self, key: str, group_by: str = None):
        """Returns the standard deviation of the specified key in the DataFrame.

        Args:
            key (str): Key to identify the column.
            group_by (str, optional): Key to group by. Defaults to None.

        Returns:
            float or pd.Series: Standard deviation value(s).
        """
        if group_by is None:
            return self[key].std()
        else:
            return self.groupby(group_by)[key].std()

    def __dropna(self):
        """Drops rows and columns with NaN values."""
        self.fillna(value=np.nan, inplace=True)
        self.dropna(axis=1, how="all", inplace=True)
        self.dropna(inplace=True)

    def __update(self):
        """Updates the 'rows' and 'cols' attributes based on DataFrame shape."""
        self.rows = self.shape[0]
        self.cols = self.shape[1]
