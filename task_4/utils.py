import cv2
import torch

from tqdm import tqdm
from torchvision import transforms
from torchmetrics.detection.mean_ap import MeanAveragePrecision


def predict(image, model, device, detection_threshold):
    transform = transforms.Compose([
        transforms.ToTensor(),
    ])
    image = transform(image).to(device)
    image = image.unsqueeze(0)
    outputs = model(image)

    pred_classes = [coco_names[i] for i in outputs[0]["labels"].cpu().numpy()]
    pred_scores = outputs[0]["scores"].detach().cpu().numpy()
    pred_bboxes = outputs[0]["boxes"].detach().cpu().numpy()
    boxes = pred_bboxes[pred_scores >= detection_threshold].astype(np.int32)

    return boxes, pred_classes, outputs[0]["labels"]

def draw_boxes(boxes, classes, labels, image):
    image = cv2.cvtColor(np.asarray(image), cv2.COLOR_BGR2RGB)
    for i, box in enumerate(boxes):
        cv2.rectangle(
            image,
            (int(box[0]), int(box[1])),
            (int(box[2]), int(box[3])),
            (0, 255, 0),
            2,
        )
        cv2.putText(
            image, classes[i], (int(box[0]), int(box[1]-5)),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.8,
            (0, 255, 0),
            2,
            lineType=cv2.LINE_AA,
        )
    return image

def validate(valid_data_loader, model, device, limit=-1):
    model.eval()
    
    prog_bar = tqdm(valid_data_loader, total=len(valid_data_loader))
    target = []
    preds = []
    for n, data in enumerate(prog_bar):
        images, targets = data
        
        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in targets]
        
        with torch.no_grad():
            outputs = model(images, targets)

        for i in range(len(images)):
            true_dict = dict()
            preds_dict = dict()
            true_dict['boxes'] = targets[i]['boxes'].detach().cpu()
            true_dict['labels'] = targets[i]['labels'].detach().cpu()
            preds_dict['boxes'] = outputs[i]['boxes'].detach().cpu()
            preds_dict['scores'] = outputs[i]['scores'].detach().cpu()
            preds_dict['labels'] = outputs[i]['labels'].detach().cpu()
            preds.append(preds_dict)
            target.append(true_dict)
        if n == limit:
            break

    metric = MeanAveragePrecision()
    metric.update(preds, target)
    metric_summary = metric.compute()
    return metric_summary