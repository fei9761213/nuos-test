import os
import torch

from PIL import Image
from pycocotools.coco import COCO
from torchvision import transforms
from torch.utils.data import Dataset

class CocoDataset(Dataset):
    def __init__(self, root, annotation):
        self.root = root
        self.coco = COCO(annotation)

        self.transforms = transforms.Compose([
            transforms.ToTensor(),
        ])
        self.ids = list(sorted(self.coco.imgs.keys()))

    def __getitem__(self, index):
        img_ids = self.ids[index]
        anno_ids = self.coco.getAnnIds(imgIds=img_ids)
        anno = self.coco.loadAnns(anno_ids)
        path = self.coco.loadImgs(img_ids)[0]['file_name']
        img = Image.open(os.path.join(self.root, path))

        num_objs = len(anno)

        boxes = []
        for i in range(num_objs):
            xmin = anno[i]['bbox'][0]
            ymin = anno[i]['bbox'][1]
            xmax = xmin + anno[i]['bbox'][2]
            ymax = ymin + anno[i]['bbox'][3]
            if (xmax - xmin > 0) and (ymax - ymin > 0):
                boxes.append([xmin, ymin, xmax, ymax])

        if (len(boxes) == 0):
            boxes = torch.as_tensor(boxes).reshape(-1, 4)
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.ones((num_objs,), dtype=torch.int64)
        img_ids = torch.tensor([img_ids])
        areas = []
        for i in range(num_objs):
            areas.append(anno[i]['area'])
        areas = torch.as_tensor(areas, dtype=torch.float32)
        iscrowd = torch.zeros((num_objs,), dtype=torch.int64)

        my_annotation = {}
        my_annotation["boxes"] = boxes
        my_annotation["labels"] = labels
        my_annotation["image_id"] = img_ids
        my_annotation["area"] = areas
        my_annotation["iscrowd"] = iscrowd

        if self.transforms is not None:
            img = self.transforms(img)

        return img, my_annotation

    def __len__(self):
        return len(self.ids)


def collate_fn(batch):
    return tuple(zip(*batch))
